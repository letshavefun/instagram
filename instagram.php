<?php
/**
 * Created by PhpStorm.
 * User: GMI
 * Date: 04/11/2018
 * Time: 15:23
 */
?>
<?php
require 'vendor/autoload.php';

class Instagram
{
    public function login($userName, $password, $debug, $truncateDebug, $conect){
        try {
            //se necessario, incluir $debug e $truncateDebug
            $login = $conect->login($userName, $password);
            //tratando autenticação de dois fatores
            if (!is_null($login) && $login->isTwoFactorRequired()) {
                $twoFactorIdentifier = $login->getTwoFactorInfo()->getTwoFactorIdentifier();
                $verificationCode = trim(fgets(STDIN));
                $login = $conect->finishTwoFactorLogin($userName,$password, $twoFactorIdentifier, $verificationCode);
            }
            $login = $conect->account->getCurrentUser();
        }catch (\Exception $e){
            echo $e->getMessage()."\n";
        }
        return $login;
    }

    public function infoProfile($username, $password,$conect){
        $debug = true;
        $truncatedDebug = false;
        $instagram = new Instagram();

        //$debug e $truncateDebug, retorna conjunto de informações do feed do usuario.
        $login = $instagram->login($username, $password, $debug, $truncatedDebug, $conect);

        //recebendo informações do perfil
        $id = $login->getUser()->getPk();
        $fullName =$login->getUser()->getFullName();
        $profilePicture = $login->getUser()->getProfilePicUrl();
        $biography = $login->getUser()->getBiography();
        $mailUser = lcfirst($login->getUser()->getEmail());
        $followingTag = $login->getUser()->getFollowingTagCount();
        $gender = $login->getUser()->getGender();
        $city = $login->getUser()->getCityName();
        $number =$login->getUser()->getPhoneNumber();
        $info = $conect->people->getInfoByName($username);
        $follower = $info->getUser()->getFollowerCount();
        $following = $info->getUser()->getFollowingCount();
        //$rankToken = \InstagramAPI\Signatures::generateUUID();

        $infoProfile = array(
            'picture'      => $profilePicture,
            'id'           => $id,
            'fullName'     => $fullName,
            'biography'    => $biography,
            'mailUser'     => $mailUser,
            'follower'     => $follower,
            'following'    => $following,
            'followingTag' => $followingTag,
            'gender'       => $gender,
            'city'         => $city,
            'phone'        => $number,
        );

        return $infoProfile;
    }

    public function follow($username, $password, $userId,$conect){
        $instagram = new Instagram();
        $login = $instagram->login($username,$password,'', '',$conect);
        $follow = $conect->people->follow($userId);
        return $follow;
    }

    public function unfollow($username, $password, $userId,$conect){
        $instagram = new Instagram();
        $login = $instagram->login($username,$password,'', '',$conect);
        $follow = $conect->people->unfollow($userId);

        return $follow;
    }

    public function like($mediaId, $username, $password,$conect){
        $instagram = new Instagram();
        $login = $instagram->login($username,$password,'', '',$conect);
        $like = $conect->media->like($mediaId);

        return $like;
    }

    public function unlike($mediaId, $username, $password,$conect){
        $instagram = new Instagram();
        $login = $instagram->login($username,$password,'', '', $conect);
        $unlike = $conect->media->unlike($mediaId);
        return $unlike;
    }

    public function comment($mediaId,$commentString, $username, $password, $conect){
        $instagram = new Instagram();
        $login = $instagram->login($username,$password,'', '', $conect);
        $comment = $conect->media->comment($mediaId,$commentString,'', '');

        return $comment;
    }

    public function uncomment($mediaId,$commentID, $username, $password, $conect){
        $instagram = new Instagram();
        $login = $instagram->login($username,$password,'', '', $conect);
        $uncomment = $conect->media->deleteComment($mediaId,$commentID);
        return $uncomment;
    }

    public function disbleComments($mediaId,$username, $password,$conect){
        $instagram = new Instagram();
        $login = $instagram->login($username,$password,'', '', $conect);
        $disableComments ="";
        try{
            $disableComments = $conect->media->disableComments($mediaId);
        }catch (\Exception $e){
            echo $e->getMessage()."\n";
        }
        return $disableComments;
    }

    public function connect(){
        $conect = new InstagramAPI\Instagram();
        return $conect;

    }

    public function id($media_url){
        $api = file_get_contents("http://api.instagram.com/oembed?callback=&url=" . $media_url);
        $media_id = json_decode($api,true)['media_id'];
        return $media_id;
    }

    public function timeline($username,$password,$conect){
        $instagram = new Instagram();
        $infoProfile = $instagram->infoProfile($username,$password,$conect);
        $timeLine = $conect->timeline->getUserFeed($infoProfile['id']);
        return $timeLine;
    }

    public function singleCollection($conect){
        $singleCollection = $conect->media->getSavedFeed();
        return $singleCollection;
    }

    public function publicacoes($username, $password, $conect){
        $instagram = new Instagram();
        $conect = $instagram->connect();
        $info = $instagram->infoProfile($username, $password,$conect);
        $publicacoes = $conect->timeline->getUserFeed($info['id']);
        return json_decode($publicacoes,false);
    }
}


