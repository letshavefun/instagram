# ![logo](/examples/assets/instagram.png) Instagram PHP [![Latest Stable Version](https://poser.pugx.org/mgp25/instagram-php/v/stable)](https://packagist.org/packages/mgp25/instagram-php) [![Total Downloads](https://poser.pugx.org/mgp25/instagram-php/downloads)](https://packagist.org/packages/mgp25/instagram-php) ![compatible](https://img.shields.io/badge/PHP%207-Compatible-brightgreen.svg) [![License](https://poser.pugx.org/mgp25/instagram-php/license)](https://packagist.org/packages/mgp25/instagram-php)

----------
## Installation

### Using Composer

```sh
composer require mgp25/instagram-php
```

```php
require __DIR__.'/../vendor/autoload.php';

$ig = new \InstagramAPI\Instagram();
```

If you want to test new and possibly unstable code that is in the master branch, and which hasn't yet been released, then you can use master instead (at your own risk):

```sh
composer require mgp25/instagram-php dev-master
```
###DATABASE

````BD
host: sql50.main-hosting.eu
banco: u741995536_insta	
usuario: u741995536_andsu
senha: gera_money$
````
###FTP

````FTP
Host	: ftp.caener.com.br
Usuário : u741995536.andsu
Senha   : gera_money$
IP FTP  : 177.234.153.44
Porta   : 21
````
